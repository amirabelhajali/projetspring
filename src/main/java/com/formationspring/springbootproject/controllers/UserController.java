package com.formationspring.springbootproject.controllers;

import com.formationspring.springbootproject.Repository.UserRepository;
import com.formationspring.springbootproject.Entities.User;
import com.formationspring.springbootproject.Services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
public class UserController {

    @Autowired
    private UserService userService;

    @RequestMapping("/")
    @ResponseBody
    public String welcome() {
        return "Welcome to USER CRUD Example.";
    }

   /* // URL:
    // http://localhost:8080/SomeContextPath/employees
    // http://localhost:8080/SomeContextPath/employees.xml
    // http://localhost:8080/SomeContextPath/employees.json
    @RequestMapping(value = "/getusers", //
            method = RequestMethod.GET) //
         //   produces = { MediaType.APPLICATION_JSON_VALUE})
    @ResponseBody
    public List<User> getUsers() {
        List<User> list = userDAO.getAllUsers();
        return list;
    }

    // URL:
    // http://localhost:8080/SomeContextPath/employee/{empNo}
    // http://localhost:8080/SomeContextPath/employee/{empNo}.xml
    // http://localhost:8080/SomeContextPath/employee/{empNo}.json
    @RequestMapping(value = "/user/{id}", //
            method = RequestMethod.GET) //
          //  produces = { MediaType.APPLICATION_JSON_VALUE) //
                  //  MediaType.APPLICATION_XML_VALUE })
    @ResponseBody
    public User getUser(@PathVariable("id") int id) {
        return userDAO.getUser(id);
    }

    // URL:
    // http://localhost:8080/SomeContextPath/employee
    // http://localhost:8080/SomeContextPath/employee.xml
    // http://localhost:8080/SomeContextPath/employee.json

    @RequestMapping(value = "/addUser", //
            method = RequestMethod.POST) //
         //   produces = { MediaType.APPLICATION_JSON_VALUE, //
        //            MediaType.APPLICATION_XML_VALUE })
    @ResponseBody
    public User addUser(@RequestBody User user) {

        System.out.println("(Service Side) Creating employee: " + user.getId());

        return userDAO.addUser(user);
    }

    // URL:
    // http://localhost:8080/SomeContextPath/employee
    // http://localhost:8080/SomeContextPath/employee.xml
    // http://localhost:8080/SomeContextPath/employee.json
    @RequestMapping(value = "/updateUser", //
            method = RequestMethod.PUT)//
          //  produces = { MediaType.APPLICATION_JSON_VALUE, //
           //         MediaType.APPLICATION_XML_VALUE })
    @ResponseBody
    public User updateUser(@RequestBody User user) {

        System.out.println("(Service Side) Editing employee: " + user.getId());

        return userDAO.updateUser(user);
    }

    // URL:
    // http://localhost:8080/SomeContextPath/employee/{empNo}
    @RequestMapping(value = "/deleteUser/{id}", //
            method = RequestMethod.DELETE)//
        //    produces = { MediaType.APPLICATION_JSON_VALUE, MediaType.APPLICATION_XML_VALUE })
    @ResponseBody
    public void deleteUser(@PathVariable("id") int id) {

        System.out.println("(Service Side) Deleting employee: " + id);

        userDAO.deleteUser(id);
    }*/

    @GetMapping("/users")
    public List<User> index() {
        return userService.getAllUsers();
    }

   @GetMapping("/user/{id}")
        public User show(@PathVariable("id") long id){
         return userService.getUser(id);
    }
   /* @PostMapping("/user/search")
    public List<User> search(@RequestBody Map<String, String> body){
        String searchTerm = body.get("text");
        return userDAO.findByTitleContainingOrContentContaining(searchTerm, searchTerm);
    }*/
    @PostMapping("/addUser")
    public void create(@RequestBody Map<String, String> body){
        String nom = body.get("nom");
        String prenom = body.get("prenom");
        String email = body.get("email");
        String password = body.get("password");
        userService.addUser(new User( nom, prenom,email, password));
    }

   @PutMapping("/updateUser/{id}")
    public  void update(@PathVariable long id, @RequestBody Map<String, String> body){

        User user = userService.getUser(id);;
        user.setNom(body.get("nom"));
        user.setPrenom(body.get("prenom"));
        user.setEmail(body.get("email"));
        user.setPassword(body.get("password"));
         userService.addUser(user);
    }

    @DeleteMapping("/deleteUser/{id}")
      public boolean delete(@PathVariable Long id){
        userService.deleteUser(id);
        return true;
 }
}
