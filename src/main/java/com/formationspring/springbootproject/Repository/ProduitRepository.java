package com.formationspring.springbootproject.Repository;

import com.formationspring.springbootproject.Entities.Produit;
import com.formationspring.springbootproject.Entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProduitRepository extends JpaRepository<Produit, Long> {
}
