package com.formationspring.springbootproject.Services;

import com.formationspring.springbootproject.Entities.User;
import com.formationspring.springbootproject.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.List;

@Service
public class UserService {

    @Autowired
    private UserRepository userRepo;


    public List<User> getAllUsers() {
        return userRepo.findAll();
    }

    public void addUser(User user)
    {
        userRepo.save(user);
    }

    public  User getUser(Long id)
    {
        return userRepo.findById(id).get();
    }

    public void deleteUser(Long id)
    {
        userRepo.deleteById(id);
    }

}
