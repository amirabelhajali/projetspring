package com.formationspring.springbootproject;

import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
public class HomePage {

    @GetMapping("/home")
    public String getHomePage()
    {
        return "Welcome Home";
    }
}
