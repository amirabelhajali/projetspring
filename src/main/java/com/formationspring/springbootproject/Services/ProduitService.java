package com.formationspring.springbootproject.Services;

import com.formationspring.springbootproject.Entities.Produit;
import com.formationspring.springbootproject.Entities.User;
import com.formationspring.springbootproject.Repository.ProduitRepository;
import com.formationspring.springbootproject.Repository.UserRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class ProduitService {
    @Autowired
    private ProduitRepository produitRepo;


    public List<Produit> getAllProduit() {
        return produitRepo.findAll();
    }

    public void addProduit(Produit produit)
    {
        produitRepo.save(produit);
    }

    public  Produit getProduit(Long id)
    {
        return produitRepo.findById(id).get();
    }

    public void deleteProduit(Long id)
    {
        produitRepo.deleteById(id);
    }
}
