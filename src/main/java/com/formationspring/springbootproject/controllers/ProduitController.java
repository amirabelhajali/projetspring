package com.formationspring.springbootproject.controllers;

import com.formationspring.springbootproject.Entities.Produit;
import com.formationspring.springbootproject.Entities.User;
import com.formationspring.springbootproject.Services.ProduitService;
import com.formationspring.springbootproject.Services.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.Map;

@RestController
public class ProduitController {
    @Autowired
    private ProduitService produitService;



    @GetMapping("/produits")
    public List<Produit> index() {
        return produitService.getAllProduit();
    }

    @GetMapping("/produit/{id}")
    public Produit show(@PathVariable("id") long id){
        return produitService.getProduit(id);
    }
    @PostMapping("/addProduit")
    public void create(@RequestBody Map<String, String> body){
        String libelle = body.get("libelle");
        String description = body.get("description");
        produitService.addProduit(new Produit( libelle, description));
    }

    @PutMapping("/updateProduit/{id}")
    public  void update(@PathVariable long id, @RequestBody Map<String, String> body){

        Produit produit = produitService.getProduit(id);;
        produit.setLibelle(body.get("libelle"));
        produit.setDescription(body.get("description"));
        produitService.addProduit(produit);
    }

    @DeleteMapping("/deleteproduit/{id}")
    public boolean delete(@PathVariable Long id){
        produitService.deleteProduit(id);
        return true;
    }
}
