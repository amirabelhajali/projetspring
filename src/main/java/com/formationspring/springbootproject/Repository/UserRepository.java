package com.formationspring.springbootproject.Repository;

import com.formationspring.springbootproject.Entities.User;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface UserRepository extends JpaRepository<User, Long> {
    //List<User> findByTitleContainingOrContentContaining(String text, String textAgain);
}
